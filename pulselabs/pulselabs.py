from typing import Any, Dict, List, Tuple, Union

import requests


class AuthenticationError(BaseException):
    pass


class APIError(BaseException):
    pass


class pulselabsAPI:
    base_url: str = "http://pulseuiapi.azurewebsites.net/api/"

    def __init__(self, username: str = "", password: str = ""):
        self.username: str = username
        self.password: str = password

        self.token: str = ""
        self.headers: dict = {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

    def get(self, url: str, **kwargs: Any) -> requests.Response:
        kwargs["headers"] = kwargs.get("headers", self.headers)

        return requests.get(self.base_url + url.lstrip("/"), **kwargs)

    def post(self, url: str, **kwargs: Any) -> requests.Response:
        kwargs["headers"] = kwargs.get("headers", self.headers)

        return requests.post(self.base_url + url.lstrip("/"), **kwargs)

    def set_token(self, token: str):
        self.token = token

        if self.token:
            self.headers["X-Authorization"] = self.token

    def authenticate(self) -> Tuple[str, int]:
        data: dict = {"User": self.username, "Password": self.password}

        res: requests.Response = self.post(
            "user/login", headers=self.headers, json=data
        )

        try:
            self.token = res.json().get("Token", "")
        except:
            self.token = ""

        if self.token:
            self.headers["X-Authorization"] = self.token

        return self.token, res.status_code

    def get_data_csv(
        self,
        device_id: Union[str, int],
        sensor_type: Union[str, int],
        **params: Union[str, int, List[Union[str, int]]],
    ) -> Tuple[str, int]:
        params["sensorType"] = sensor_type

        res: requests.Response = self.get(
            f"devices/{device_id}/Download", headers=self.headers, params=params
        )

        try:
            return res.text, res.status_code
        except:
            return "", res.status_code

    def get_data_list(
        self,
        device_id: Union[str, int],
        sensor_type: Union[str, int],
        **params: Union[str, int, List[Union[str, int]]],
    ) -> Tuple[dict, int]:
        params["sensorType"] = sensor_type

        res: requests.Response = self.get(
            f"devices/{device_id}/readings/ByDate", headers=self.headers, params=params
        )

        try:
            return res.json(), res.status_code
        except:
            return {}, res.status_code

    def post_data(
        self,
        device_id: Union[str, int],
        data_type: Union[str, int],
        data: Dict[str, Union[str, int]],
    ) -> int:
        payload: dict = {"Id": data_type, "Value": data["Value"], "Device": device_id}

        if "Time" in data:
            payload["Time"] = data["Time"]

        res: requests.Response = self.post(
            f"messages", headers=self.headers, json=payload
        )

        return res.status_code
